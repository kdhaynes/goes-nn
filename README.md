# Low Cloud and Icing Detection from the Geostationary Operational Environmental Satellite (GOES-16) Using Neural Networks

* Uses Python in a Jupyter Notebook (Low-Cloud-Icing-NN.ipynb)
  - Libraries Required: copy, gc, math, matplotlib, netCDF4, numpy, os, pandas, sys, time, torch, psutil, humanize, os, GPUtil

- Includes presentation slides (Low-Cloud-Icing-NN.pdf)
